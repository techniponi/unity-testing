﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Entity : MonoBehaviour {

    public Text healthText;

    public int health;

	void Start () {
        health = 5;
	}
	
	void Update () {
        healthText.text = health.ToString();
	}
}
