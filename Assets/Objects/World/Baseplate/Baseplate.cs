﻿using UnityEngine;
using System.Collections;

public class Baseplate : MonoBehaviour {

    // global vars
    int clickNum = 0;

	void Start () {
	
	}
	
	void Update () {
        if (Input.GetMouseButtonDown(0))
        {
            if (checkClicked())
            {
                clickNum++;
                Debug.Log(clickNum);
            }
        }
    }

    bool checkClicked() // does not check for specific object, only if an object exists.
    {
        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(ray, out hit))
        {
            if (hit.transform != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        return false;
    }
}
